# practice-site

A website for practicing web development created by tutorial Web Development with Python Tutorial – Flask & Dynamic Database-Driven Web Apps on freeCodeCamp.org

# Progress Checklist

## Step 1 - Project Setup & Flask Basics - Status: _Done_

- [x] Create a project on GitHub
- [x] Open up the project on Replit
- [x] Create and run a Flask web server
- [x] Push changes back to GitHub

## Step 2 - Web Pages with HTML & CSS - Status: _Done_

- [x] Render templates and use static assets
- [x] Create the layout of the page using HTML tags
- [x] Style the page using CSS classes, properties, and values
- [x] Use the Bootstrap framework for faster development

## Step 3 - Dynamic Data & Cloud Deployment - Status: _Done_

- [x] Render dynamic data using Jinja template tags
- [x] Add an API route to return JSON
- [x] Deploy the project to Render.com 
- [x] Connect a domain with Render deployment

## Step 4 - Functional & Aesthetic Improvements - Status: _Done_

- [x] Add a Navbar and Footer from Bootstrap
- [x] Add `mailto:` links for the buttons
- [x] Make the website mobile-friendly (responsive)
- [x] Refactor templates into reusable components

## Future Work - Status: _open_

- [ ] Create a page to show the details about the job (requirements etc.)
- [ ] Create a page to submit an application (instead of sending an email)
- [ ] Store information about jobs and applications in a cloud database
- [ ] Send email confirmation to candidate & Jovian admin after application
- [ ] Create an admin login interface to check submitted applications

You can go ahead and try the above steps with the help of bootstrap documentation. 

# Rescources

* Main tutorial: https://youtu.be/yBDHkveJUf4
* Html guide: https://htmldog.com/guides/html/beginner/tags/
* CSS guide: https://jovian.com/outlink?url=https%3A%2F%2Fhtmldog.com%2Fguides%2Fcss%2F
* Bootstrap: https://getbootstrap.com/docs/5.3/getting-started/introduction/
* Deploy Flask on render.com: https://render.com/docs/deploy-flask

# Tools

* Hosting: https://render.com
* Mailto link generator: https://mailtolink.me/

# ToDo

- [ ] Setting the Favicon using a Flask template @https://youtu.be/yBDHkveJUf4?t=1632
- [ ] Learn basic html: https://htmldog.com/guides/
- [ ] Learn basic css: https://web.dev/learn/css/