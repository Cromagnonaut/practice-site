from flask import Flask, render_template, jsonify

app = Flask(__name__)

JOBS = [
    {
        'id': 1,
        'title': 'System Administrator',
        'location': 'Berlin, Germany',
        'salary': '80.000 €',
    },
    {
        'id': 2,
        'title': 'Network manager',
        'location': 'Paris, France',
        'salary': '60.000 €',
    },
    {
        'id': 3,
        'title': 'Security Advisor',
        'location': 'Remote',
    },
    {
        'id': 4,
        'title': 'Software Developer',
        'location': 'Remote',
        'salary': '100.000 $',
    },
]

@app.route("/")
def hello_world(): # Defining a function that returns "Hello, world"
    return render_template('home.html',
                            jobs=JOBS,
                            company_name='Homelab')

# Create JSON "API" route
@app.route("/jobs")
def list_jobs():
    return jsonify(JOBS) 

print(__name__)
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True) # Start a local development server